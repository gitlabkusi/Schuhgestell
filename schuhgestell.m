total_height = 1800
thickness_end = 25
thickness_mid = 19
num_holes = 8
height_hole = 200

%height_hole = (total_height - 2*thickness_end - thickness_mid*(num_holes-1)) / num_holes



total_height = thickness_end + thickness_mid*(num_holes-1) + num_holes*height_hole



fussleiste_h = 70;
tablar_unterstes_h = 25
tablar_mittlere_h = 19
tablar_oberstes_h = 25
loch_h = 200

tablar_mittlere_pos_z = fussleiste_h + tablar_unterstes_h + loch_h + (loch_h + tablar_mittlere_h).* (0:6)


seitenwand_h = (loch_h+tablar_mittlere_h)*7 + loch_h+tablar_oberstes_h
tablar_oberstes_pos_z = fussleiste_h + tablar_unterstes_h + seitenwand_h - tablar_oberstes_h


%% Dübellöcher für Tablare, Ursprung am unteren Brettrand der Seitenwand

% ursprung am unteren Brettrand der Seitenwand
tablar_mitte_z = loch_h + tablar_mittlere_h/2 + (loch_h + tablar_mittlere_h) .* (0:6);

% oberstes tablar ist dicker
tablar_mitte_z = [tablar_mitte_z   tablar_mitte_z(end) + tablar_mittlere_h/2 + loch_h + tablar_oberstes_h/2]